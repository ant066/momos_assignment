import './styles.scss'
import React from 'react'

export const Layout = ({ children }) => {
  return <div className="main">{children}</div>
}

export default Layout
