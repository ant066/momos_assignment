import './styles.scss'
import React, { useContext, useState } from 'react'
import Task from '../task'
import { Draggable, Droppable } from 'react-beautiful-dnd'
import { AppContext } from '../../app'
import { ADD_LIST } from '../../app/store/actions'

export const List = ({ title, id, tasks, isNew, onClose }) => {
  const [isAddTask, setAddTask] = useState(false)
  const [value, setValue] = useState('')
  const { dispatch } = useContext(AppContext)

  const onChange = (evt) => {
    const { value } = evt.target
    setValue(value)
  }

  const onSave = () => {
    dispatch({
      type: ADD_LIST,
      payload: {
        title: value,
      },
    })
    onClose()
  }

  const onCloseAddTask = () => {
    setAddTask(false)
  }
  const onAddNewTask = () => {
    setAddTask(true)
  }

  if (isNew) {
    return (
      <div className="list">
        <div className="new-list">
          <input value={value} onChange={onChange}></input>
          <div className="control">
            <button onClick={onSave} className="save">
              Save
            </button>
            <button onClick={onClose} className="cancel">
              Cancel
            </button>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className="wrapper">
      <div className="title">
        {title}
        <button onClick={onAddNewTask}>ADD TASK</button>
      </div>
      <Droppable droppableId={id}>
        {(provided) => (
          <div ref={provided.innerRef} {...provided.droppableProps} className="list">
            {isAddTask && <Task isNew listId={id} onClose={onCloseAddTask} />}

            {tasks.map((task, idx) => {
              return (
                <Draggable key={task.id} draggableId={task.id} index={idx}>
                  {(provided, snapshot) => (
                    <Task title={task.title} content={task.content} provided={provided} snapshot={snapshot} />
                  )}
                </Draggable>
              )
            })}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </div>
  )
}

export default List
